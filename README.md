# RHSSO Login Backend Sample Project
## _Date: February 17, 2021_
## _Author: Nevin Zhu_

There are two components to this demo. 

- rhsso-login-frontend
- rhsso-login-backend

Please refer to README.md in rhsso-login-frontend for instructions on installation and testing.

This project is a node.js application showing how to integrate with keycloak and authorization services using role validation. Following are the key components for reference:

- app.js
- keycloak.json


## Feature

This project contains a web form which allows user to perform actions such as list and insert. 
There are three routes defined: 

- listItemRoute is for listing the data in the web form which requires list-reader role.
- canInsertRoute is for displaying the EDIT button on the UI which requires list-writer role.
- InsertItemRoute is for inserting data into the web form which requires the list-writer role.  

list-reader, list-writer must be defined in Keycloak web console and assign to user

## Reference Code

- app.js Line 60 to 65 shows how to wrap the call with keycloak protection API
- app.js Line 20-22 shows how to bootstrap keycloak.json
- keycloak.json contains connection configuration for realm